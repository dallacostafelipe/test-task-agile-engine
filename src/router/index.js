import Vue from 'vue'
import Router from 'vue-router'

const HomeView = () => import('@/views/Home')
const DetailsView = () => import('@/views/Details')

Vue.use(Router)

export default new Router({
  mode: 'hash',
  linkActiveClass: 'active',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
})

function configRoutes () {
    return [
        {
            path: '/',
            name: '',
            redirect: '/images',
        },
        {
            path: '/images',
            name: 'images',
            component: HomeView,
        },
        {
            path: '/images/:id',
            name: 'details',
            component: DetailsView,
        }
    ]
}

