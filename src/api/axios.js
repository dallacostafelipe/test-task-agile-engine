import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://interview.agileengine.com'
});

instance.interceptors.response.use(
    (response) => {
        return response
    },
    async function (err) {
        const originalRequest = err.config;
        if (err.response.status === 401 && !originalRequest._retry) {
            originalRequest._retry = true;
            const token = await getToken();
            instance.defaults.headers.common['Authorization'] = token;
            return instance(originalRequest);
        }
        return Promise.reject(err);
    }
);

async function getToken () {
    let response = await axios.post(
        'http://interview.agileengine.com/auth',
        { "apiKey": process.env.VUE_APP_API_KEY }
    )
    return response.data.token
}

export default instance;