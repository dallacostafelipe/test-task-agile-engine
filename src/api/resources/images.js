import axios from '@/api/axios'

export default {
    get: async (page) => {
        let response = await axios.get(
            `${axios.defaults.baseURL}/images?page=${page}`
        )
        return response.data
    },

    getById: async (id) => {
        let response = await axios.get(
            `${axios.defaults.baseURL}/images/${id}`
        )
        return response.data
    }

}